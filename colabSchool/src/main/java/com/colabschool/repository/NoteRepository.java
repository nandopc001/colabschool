/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Note;

/**
 * @author nandopc001
 *
 */
public interface NoteRepository extends JpaRepository<Note, Long> {

}
