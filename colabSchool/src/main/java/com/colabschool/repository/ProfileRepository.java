/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Profile;

/**
 * @author nandopc001
 *
 */
public interface ProfileRepository extends JpaRepository<Profile, Long> {

	Profile findByDescription(String description);
}
