/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Message;

/**
 * @author nandopc001
 *
 */
public interface MessageRepository extends JpaRepository<Message, Long> {

}
