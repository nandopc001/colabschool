/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Priority;

/**
 * @author nandopc001
 *
 */
public interface PriorityRepository extends JpaRepository<Priority, Long> {

}
