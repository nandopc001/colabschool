/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.ResponsibleStudent;

/**
 * @author nandopc001
 *
 */
public interface ResponsibleStudentRepository extends JpaRepository<ResponsibleStudent, Long> {

}
