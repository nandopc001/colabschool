/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Role;

/**
 * @author nandopc001
 *
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

}
