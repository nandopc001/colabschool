/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Adress;

/**
 * @author nandopc001
 *
 */
public interface AdressRepository extends JpaRepository<Adress, Long> {

}
