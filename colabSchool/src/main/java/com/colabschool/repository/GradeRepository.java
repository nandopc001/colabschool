/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Grade;

/**
 * @author nandopc001
 *
 */
public interface GradeRepository extends JpaRepository<Grade, Long> {

}
