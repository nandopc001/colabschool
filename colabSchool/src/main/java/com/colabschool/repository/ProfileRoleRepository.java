/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.ProfileRole;

/**
 * @author nandopc001
 *
 */
public interface ProfileRoleRepository extends JpaRepository<ProfileRole, Long> {

}
