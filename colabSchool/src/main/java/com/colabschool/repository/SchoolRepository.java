/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.School;

/**
 * @author nandopc001
 *
 */
public interface SchoolRepository extends JpaRepository<School, Long> {

}
