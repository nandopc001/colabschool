/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.User;

/**
 * @author nandopc001
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

	User findByName(String name);

	User findByEmail(String email);

	User findByIdUser(Long idUser);

}
