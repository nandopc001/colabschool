/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Teacher;

/**
 * @author nandopc001
 *
 */
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

}
