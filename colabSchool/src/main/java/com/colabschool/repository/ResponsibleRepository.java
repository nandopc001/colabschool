/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Responsible;

/**
 * @author nandopc001
 *
 */
public interface ResponsibleRepository extends JpaRepository<Responsible, Long> {

}
