/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.ParentSubsidiary;

/**
 * @author nandopc001
 *
 */
public interface ParentSubsidiaryRepository extends JpaRepository<ParentSubsidiary, Long> {

}
