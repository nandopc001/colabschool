/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.UserMessage;

/**
 * @author nandopc001
 *
 */
public interface UserMessageRepository extends JpaRepository<UserMessage, Long> {

}
