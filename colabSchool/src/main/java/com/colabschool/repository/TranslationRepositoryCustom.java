/**
 * 
 */
package com.colabschool.repository;

/**
 * @author nandopc001
 *
 */
public interface TranslationRepositoryCustom {

	String findTextByIdLangByIdScreen(String keyTranslation, String keyScreen, String locale);

}
