/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Student;

/**
 * @author nandopc001
 *
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

}
