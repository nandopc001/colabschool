/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Classe;

/**
 * @author nandopc001
 *
 */
public interface ClasseRepository extends JpaRepository<Classe, Long> {

}
