/**
 * 
 */
package com.colabschool.repository.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.colabschool.repository.TranslationRepositoryCustom;

/**
 * @author nandopc001
 *
 */
@Repository
public class TranslationRepositoryCustomImpl implements TranslationRepositoryCustom {
	@PersistenceContext
	protected EntityManager em;

	public String findTextByIdLangByIdScreen(String keyTranslation, String keyScreen, String locale) {

		/*
		 * Criteria criteria =
		 * em.unwrap(Session.class).createCriteria(Translation.class); criteria
		 * = criteria.createCriteria("languages"); //criteria =
		 * criteria.createCriteria("screens");
		 * 
		 * Criterion languagePT = Restrictions.eq("locale", "pt_br");
		 * //Criterion screenCad = Restrictions.eq("keyScreen", "CAD_SCREEN");
		 * //criteria.add(Restrictions.and(languagePT, screenCad));
		 * criteria.add(languagePT); //criteria.add(screenCad);
		 * List<Translation> result = criteria.list();
		 */
		Query query = em.unwrap(Session.class)
				.createSQLQuery(
						"SELECT t.text FROM colab_school.translation t inner join colab_school.language_translation lt on t.id_translation = lt.id_translation inner join colab_school.screen_translation st on t.id_translation = st.id_translation inner join colab_school.language l on lt.id_language = l.id_language inner join colab_school.screen s on st.id_screen = s.id_screen where t.key_translation = :key_translation and s.key_screen = :key_screen and l.locale = :locale")
				.setParameter("key_translation", keyTranslation).setParameter("key_screen", keyScreen)
				.setParameter("locale", locale);
		List<String> result = query.list();
		return result.get(0);

	}

}
