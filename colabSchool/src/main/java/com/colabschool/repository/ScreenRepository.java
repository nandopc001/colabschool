/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Screen;

/**
 * @author nandopc001
 *
 */
public interface ScreenRepository extends JpaRepository<Screen, Long> {

	Screen findByKeyScreen(String keyScreen);

}
