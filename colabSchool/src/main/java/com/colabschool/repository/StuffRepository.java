package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Stuff;

public interface StuffRepository extends JpaRepository<Stuff, Long> {

}
