/**
 * 
 */
package com.colabschool.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.colabschool.model.Contact;

/**
 * @author nandopc001
 *
 */
public interface ContactRepository extends JpaRepository<Contact, Long> {

	Contact findByEmail(String email);

}
