/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Adress;
import com.colabschool.model.Contact;
import com.colabschool.model.ParentSubsidiary;
import com.colabschool.model.Profile;
import com.colabschool.model.User;
import com.colabschool.service.AdressService;
import com.colabschool.service.ContactService;
import com.colabschool.service.ParentSubsidiaryService;
import com.colabschool.service.ProfileService;
import com.colabschool.service.ScreenService;
import com.colabschool.service.UserService;

/**
 * @author nandopc001
 *
 */
@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private AdressService adressService;

	@Autowired
	private ContactService contactService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private ParentSubsidiaryService parentSubsidiaryService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ScreenService screenService;

	@RequestMapping(value = "/cad_user", method = RequestMethod.GET)
	public ModelAndView cadUser() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		Contact contact = new Contact();
		Adress adress = new Adress();
		ParentSubsidiary parentSubsidiary = new ParentSubsidiary();
		String label = messageSource.getMessage("/cad_user", null, LocaleContextHolder.getLocale());
		String form = "form_cad_user";
		Profile profile = new Profile();
		modelAndView.addObject("user", user);
		modelAndView.addObject("parentSubsidiarys", parentSubsidiaryService.findAll());
		modelAndView.addObject("profiles", profileService.findAll());
		modelAndView.addObject("contact", contact);
		modelAndView.addObject("fieldTypeCad", label);
		modelAndView.addObject("form", form);
		modelAndView.addObject("parentSubsidiary", parentSubsidiary);
		modelAndView.addObject("profile", profile);
		modelAndView.addObject("adress", adress);
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.setViewName("cad_user");
		return modelAndView;
	}

	@RequestMapping(value = "/cad_user", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, @Valid ParentSubsidiary parentSubsidiary, @Valid Adress adress,
			@Valid Profile profile, @Valid Contact contact, BindingResult result) {
		if (result.hasErrors()) {
			return cadUser();
		}
		if (profile != null && adress != null && contact != null && parentSubsidiary != null && user != null) {
			contact = contactService.save(contact);
			adress = adressService.save(adress);
			user.setProfile(profile);
			user.setContact(contact);
			user.setAdress(adress);
			user.setParentSubsidiary(parentSubsidiary);
			userService.save(user);
		}
		return cadUser();
	}
}
