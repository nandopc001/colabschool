/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Grade;
import com.colabschool.model.Screen;
import com.colabschool.service.GradeService;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class GradeController {

	@Autowired
	private GradeService gradeService;

	@Autowired
	private ScreenService screenService;

	@RequestMapping(value = "/cad_grade", method = RequestMethod.GET)
	public ModelAndView cadGrade() {
		ModelAndView modelAndView = new ModelAndView();
		Grade grade = new Grade();
		Screen screen = screenService.findByKeyScreen("cad_grade");
		modelAndView.addObject("grade", grade);
		modelAndView.addObject("grades", gradeService.findAll());
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.addObject("form", screen.getForm());
		modelAndView.addObject("dataTable", screen.getDataTable());
		modelAndView.setViewName("register");
		return modelAndView;
	}

	@RequestMapping(value = "/cad_grade", method = RequestMethod.POST)
	public ModelAndView createNewRole(@Valid Grade grade, BindingResult result) {
		if (result.hasErrors()) {
			return cadGrade();
		}
		if (grade != null) {
			gradeService.save(grade);
		}
		return cadGrade();
	}
}
