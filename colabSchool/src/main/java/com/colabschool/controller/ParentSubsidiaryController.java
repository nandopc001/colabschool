/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.constants.MessageReturn;
import com.colabschool.model.Adress;
import com.colabschool.model.Contact;
import com.colabschool.model.ParentSubsidiary;
import com.colabschool.model.School;
import com.colabschool.service.AdressService;
import com.colabschool.service.ContactService;
import com.colabschool.service.ParentSubsidiaryService;
import com.colabschool.service.SchoolService;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class ParentSubsidiaryController {

	@Autowired
	private ContactService contactService;

	@Autowired
	private AdressService adressService;

	@Autowired
	private ParentSubsidiaryService parentSubsidiaryService;

	@Autowired
	private ScreenService screenService;

	@Autowired
	private SchoolService schoolService;

	@RequestMapping(value = "/cad_parent_subsid", method = RequestMethod.GET)
	public ModelAndView cadParentSubsid(boolean status) {
		ModelAndView modelAndView = new ModelAndView();
		ParentSubsidiary parentSubsidiary = new ParentSubsidiary();
		Adress adress = new Adress();
		Contact contact = new Contact();
		School school = new School();
		modelAndView.addObject(parentSubsidiary);
		modelAndView.addObject("schools", schoolService.findAll());
		modelAndView.addObject(adress);
		modelAndView.addObject(contact);
		modelAndView.addObject(school);
		modelAndView.addObject("screens", screenService.findAll());
		if (status) {
			modelAndView.addObject("status", MessageReturn.SUCESS.code());
		} else {
			modelAndView.addObject("status", MessageReturn.ERROR.code());
		}

		modelAndView.setViewName("cad_parent_subsid");

		return modelAndView;
	}

	@RequestMapping(value = "/cad_parent_subsid", method = RequestMethod.POST)
	public ModelAndView createNewParentSubsid(@Valid ParentSubsidiary parentSubsidiary, @Valid School school,
			@Valid Adress adress, @Valid Contact contact, BindingResult result) {
		if (result.hasErrors()) {
			return cadParentSubsid(false);
		}
		if (adress != null && contact != null && parentSubsidiary != null && school != null) {
			Adress adressAux = adressService.save(adress);
			Contact contactAux = contactService.save(contact);
			parentSubsidiary.setSchool(schoolService.findOne(school.getIdSchool()));
			parentSubsidiary.setAdress(adressAux);
			parentSubsidiary.setIdContact(contactAux.getIdContact());
			parentSubsidiaryService.save(parentSubsidiary);
		}
		return cadParentSubsid(true);
	}
}
