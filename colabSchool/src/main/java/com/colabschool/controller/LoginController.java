/**
 * 
 */
package com.colabschool.controller;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Adress;
import com.colabschool.model.Contact;
import com.colabschool.model.Profile;
import com.colabschool.model.User;
import com.colabschool.service.AdressService;
import com.colabschool.service.ContactService;
import com.colabschool.service.ProfileService;
import com.colabschool.service.ScreenService;
import com.colabschool.service.UserService;

/**
 * @author nandopc001
 *
 */
@Controller
public class LoginController {

	@Autowired
	private UserService userService;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private AdressService adressService;

	@Autowired
	private ContactService contactService;

	@Autowired
	private ScreenService screenService;

	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		Contact contact = new Contact();
		// List<Screen> urls = screenService.findAll();
		modelAndView.addObject(user);
		modelAndView.addObject("contact", contact);
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.setViewName("index");
		return modelAndView;
	}

	@RequestMapping(value = "/blank", method = RequestMethod.GET)
	public ModelAndView blank() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("blank");
		return modelAndView;
	}
	/*
	 * @RequestMapping(value={"/", "/login"}, method = RequestMethod.GET) public
	 * ModelAndView login(){ ModelAndView modelAndView = new ModelAndView();
	 * User user = new User(); Contact contact = new Contact();
	 * modelAndView.addObject(user); modelAndView.addObject("contact",contact);
	 * modelAndView.setViewName("login"); return modelAndView; }
	 */

	/*
	 * @RequestMapping(value="/login", method = RequestMethod.POST) public
	 * ModelAndView login(@Valid User user, BindingResult result) { if
	 * (result.hasErrors()) { return index(user); } ModelAndView mv = new
	 * ModelAndView("/login");
	 * mv.addObject("user",userService.findUserByEmail(user.getEmail())); return
	 * mv;
	 * 
	 * }
	 */

	@RequestMapping(value = "/forms", method = RequestMethod.GET)
	public ModelAndView form() {
		ModelAndView modelAndView = new ModelAndView();
		// User user = new User();
		// Contact contact = new Contact();
		// modelAndView.addObject("user", user);
		// modelAndView.addObject("contact",contact);
		modelAndView.setViewName("forms");
		return modelAndView;
	}

	/*
	 * @RequestMapping(value="/cad_school", method = RequestMethod.GET) public
	 * ModelAndView cadSchool(){ ModelAndView modelAndView = new ModelAndView();
	 * //User user = new User(); //Contact contact = new Contact();
	 * //modelAndView.addObject("user", user);
	 * //modelAndView.addObject("contact",contact);
	 * modelAndView.setViewName("cad_school"); return modelAndView; }
	 */

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		Contact contact = new Contact();
		modelAndView.addObject("user", user);
		modelAndView.addObject("contact", contact);
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, @Valid Contact contact, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		// Contact contact = contactService.findContactByEmail(user.getEmail());

		if (contact == null) {
			contact = new Contact();
			contact.setEmail(user.getEmail());
			contact = contactService.save(contact);
		} else {
			contact = contactService.save(contact);
		}
		Adress adress = new Adress();
		adress.setCity("Campinas");
		adress.setComplement("ap 301 bl B");
		adress.setCountry("Brasil");
		adress.setDistrict("Jardim Nova Europa");
		adress.setNumber(new BigDecimal(75));
		adress = adressService.save(adress);
		Profile profile = profileService.findByDescription("ADMIN");
		if (profile == null) {
			profile = new Profile();
			profile.setDescription("ADMIN");
			profile = profileService.save(profile);
		}
		if (contact != null) {
			User userExists = userService.findUserByEmail(contact.getEmail());
			if (userExists != null) {
				bindingResult.rejectValue("email", "error.user",
						"There is already a user registered with the email provided");
			}
			user.setContact(contact);
			user.setAdress(adress);
			user.setProfile(profile);
		}

		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("login");
		} else {
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("user", new User());
			modelAndView.setViewName("login");

		}
		return modelAndView;
	}

	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = userService.findUserByIdUser(
				contactService.findContactByEmail(auth.getName()).getUsers().iterator().next().getIdUser());
		modelAndView.addObject("userName", "Welcome " + user.getName() + " (" + user.getEmail() + ")");
		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
		modelAndView.setViewName("/home");
		return modelAndView;
	}

}
