/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Screen;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class ScreenController {

	@Autowired
	private ScreenService screenService;

	@RequestMapping(value = "/cad_screen", method = RequestMethod.GET)
	public ModelAndView cadScreen() {
		ModelAndView modelAndView = new ModelAndView();
		Screen screen = screenService.findByKeyScreen("cad_screen");

		modelAndView.addObject("screen", screen);
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.addObject("form", screen.getForm());
		modelAndView.addObject("url", screen.getUrl());
		modelAndView.addObject("dataTable", screen.getDataTable());
		modelAndView.setViewName("register");
		return modelAndView;
	}

	@RequestMapping(value = "/cad_screen", method = RequestMethod.POST)
	public ModelAndView cadScreen(@Valid Screen screen, BindingResult result) {
		if (result.hasErrors()) {
			return cadScreen();
		}
		if (screen != null) {
			screenService.save(screen);
		}
		return cadScreen();
	}
}
