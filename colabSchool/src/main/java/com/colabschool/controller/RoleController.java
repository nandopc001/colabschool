/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Role;
import com.colabschool.service.RoleService;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class RoleController {

	@Autowired
	private RoleService roleService;

	@Autowired
	private ScreenService screenService;

	@RequestMapping(value = "/cad_role", method = RequestMethod.GET)
	public ModelAndView cadRole() {
		ModelAndView modelAndView = new ModelAndView();
		Role role = new Role();
		modelAndView.addObject("role", role);
		modelAndView.addObject("roles", roleService.findAll());
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.setViewName("cad_role");
		return modelAndView;
	}

	@RequestMapping(value = "/cad_role", method = RequestMethod.POST)
	public ModelAndView createNewRole(@Valid Role role, BindingResult result) {
		if (result.hasErrors()) {
			return cadRole();
		}
		if (role != null) {
			roleService.save(role);
		}
		return cadRole();
	}
}
