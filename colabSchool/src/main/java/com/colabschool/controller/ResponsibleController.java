/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Adress;
import com.colabschool.model.Contact;
import com.colabschool.model.ParentSubsidiary;
import com.colabschool.model.Profile;
import com.colabschool.model.Responsible;
import com.colabschool.model.Screen;
import com.colabschool.model.User;
import com.colabschool.service.ParentSubsidiaryService;
import com.colabschool.service.ProfileService;
import com.colabschool.service.ResponsibleService;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class ResponsibleController {

	@Autowired
	private ProfileService profileService;

	@Autowired
	private ParentSubsidiaryService parentSubsidiaryService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ResponsibleService responsibleService;

	@Autowired
	private ScreenService screenService;

	@RequestMapping(value = "/cad_responsible", method = RequestMethod.GET)
	public ModelAndView cadResponsible() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		Contact contact = new Contact();
		Adress adress = new Adress();
		ParentSubsidiary parentSubsidiary = new ParentSubsidiary();
		String label = messageSource.getMessage("/cad_responsible", null, LocaleContextHolder.getLocale());
		Profile profile = new Profile();
		Screen screen = screenService.findByKeyScreen("cad_responsible");
		modelAndView.addObject("user", user);
		modelAndView.addObject("parentSubsidiarys", parentSubsidiaryService.findAll());
		modelAndView.addObject("profiles", profileService.findAll());
		modelAndView.addObject("contact", contact);
		modelAndView.addObject("fieldTypeCad", label);
		modelAndView.addObject("parentSubsidiary", parentSubsidiary);
		modelAndView.addObject("profile", profile);
		modelAndView.addObject("adress", adress);
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.addObject("form", screen.getForm());
		modelAndView.addObject("url", screen.getUrl());
		modelAndView.setViewName("register");
		return modelAndView;
	}

	@RequestMapping(value = "/cad_responsible", method = RequestMethod.POST)
	public ModelAndView cadResponsible(@Valid User user, @Valid ParentSubsidiary parentSubsidiary, @Valid Adress adress,
			@Valid Profile profile, @Valid Contact contact, BindingResult result) {
		if (result.hasErrors()) {
			return cadResponsible();
		}
		if (profile != null && adress != null && contact != null && parentSubsidiary != null && user != null) {
			Responsible responsible = new Responsible();
			user.setProfile(profile);
			user.setContact(contact);
			user.setAdress(adress);
			user.setParentSubsidiary(parentSubsidiary);
			responsible.setUser(user);
			responsibleService.save(responsible);
		}
		return cadResponsible();
	}

	/*
	 * @RequestMapping(value = "/cad_student", method = RequestMethod.POST)
	 * public ModelAndView cadStudent(@Valid Student student, BindingResult
	 * result) { if (result.hasErrors()) { return cadStudent(); } if (screen !=
	 * null) { screenService.save(screen); } return cadStudent(); }
	 */
}
