/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Classe;
import com.colabschool.model.Grade;
import com.colabschool.model.Screen;
import com.colabschool.service.ClasseService;
import com.colabschool.service.GradeService;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class ClasseController {

	@Autowired
	private ClasseService classeService;

	@Autowired
	private ScreenService screenService;

	@Autowired
	private GradeService gradeService;

	@RequestMapping(value = "/cad_classe", method = RequestMethod.GET)
	public ModelAndView cadClasse() {
		ModelAndView modelAndView = new ModelAndView();
		Classe classe = new Classe();
		Screen screen = screenService.findByKeyScreen("cad_classe");
		modelAndView.addObject("classe", classe);
		modelAndView.addObject("classes", classeService.findAll());
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.addObject("grades", gradeService.findAll());
		modelAndView.addObject("form", screen.getForm());
		modelAndView.addObject(new Grade());
		modelAndView.addObject("dataTable", screen.getDataTable());
		modelAndView.setViewName("register");
		return modelAndView;
	}

	@RequestMapping(value = "/cad_classe", method = RequestMethod.POST)
	public ModelAndView cadClasse(@Valid Classe classe, @Valid Grade grade, BindingResult result) {
		if (result.hasErrors()) {
			return cadClasse();
		}
		if (classe != null) {
			classeService.save(classe);
		}
		return cadClasse();
	}

}
