/**
 * 
 */
package com.colabschool.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.School;
import com.colabschool.service.SchoolService;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class SchoolController {

	@Autowired
	private SchoolService schoolService;

	@Autowired
	private ScreenService screenService;
	/*
	 * @GetMapping("/cad_school") public ModelAndView index(School school) {
	 * ModelAndView mv = new ModelAndView("/index"); //
	 * mv.addObject("school",schoolService.findOne(new Long(4)));
	 * mv.addObject("school", school); return mv; }
	 */

	@RequestMapping(value = "/cad_school", method = RequestMethod.GET)
	public ModelAndView cadSchool() {
		ModelAndView modelAndView = new ModelAndView();
		School school = new School();
		modelAndView.addObject(school);
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.setViewName("cad_school");
		return modelAndView;
	}

	@GetMapping("/school")
	public ModelAndView findAll() {
		ModelAndView mv = new ModelAndView("/school");
		mv.addObject("schools", schoolService.findAll());
		return mv;
	}

	@GetMapping("/add")
	public ModelAndView add(School school) {
		ModelAndView mv = new ModelAndView("/addSchool");
		mv.addObject("schools", school);
		return mv;
	}

	@GetMapping("/edit/{idSchool}")
	public ModelAndView edit(@PathVariable("idSchool") Long idSchool) {
		return add(schoolService.findOne(idSchool));
	}

	@GetMapping("/delete/{idSchool}")
	public ModelAndView delete(@PathVariable("idSchool") Long idSchool) {
		schoolService.delete(idSchool);
		return findAll();
	}

	@PostMapping("/save_school")
	public ModelAndView save(@Valid School school, BindingResult result) {
		if (result.hasErrors()) {
			return add(school);
		}
		schoolService.save(school);
		return findAll();
	}

	@PostMapping("/add")
	public ModelAndView add(@Valid School school, BindingResult result) {
		if (result.hasErrors()) {
			return add(school);
		}
		schoolService.save(school);
		return findAll();
	}
}
