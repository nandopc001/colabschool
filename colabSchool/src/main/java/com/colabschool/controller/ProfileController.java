/**
 * 
 */
package com.colabschool.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.colabschool.model.Profile;
import com.colabschool.model.Role;
import com.colabschool.service.ProfileService;
import com.colabschool.service.RoleService;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Controller
public class ProfileController {

	@Autowired
	private ProfileService profileService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private ScreenService screenService;

	@RequestMapping(value = "/cad_profile", method = RequestMethod.GET)
	public ModelAndView cadProfile() {
		ModelAndView modelAndView = new ModelAndView();
		Profile profile = new Profile();
		modelAndView.addObject("roles", roleService.findAll());
		modelAndView.addObject("profile", profile);
		modelAndView.addObject("screens", screenService.findAll());
		modelAndView.setViewName("cad_profile");
		return modelAndView;
	}

	@RequestMapping(value = "/cad_profile", method = RequestMethod.POST)
	public ModelAndView createNewProfile(@Valid Profile profile,
			@RequestParam(value = "checkbox", required = false) long[] checkbox, BindingResult result) {
		if (result.hasErrors()) {
			return cadProfile();
		}
		List<Role> roles = new ArrayList<Role>();
		if (checkbox != null && profile != null) {
			for (int i = 0; i < checkbox.length; i++) {
				roles.add(roleService.findOne(checkbox[i]));

			}
			if (!roles.isEmpty()) {
				profile.setRoles(roles);
			}

			profileService.save(profile);
		}
		return cadProfile();
	}
}
