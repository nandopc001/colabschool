package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the priority database table.
 * 
 */
@Entity
@Table(name = "priority")
@NamedQuery(name = "Priority.findAll", query = "SELECT p FROM Priority p")
public class Priority implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_priority", unique = true, nullable = false)
	private Long idPriority;

	@Column(nullable = false, length = 20)
	private String description;

	// bi-directional many-to-one association to Message
	@OneToMany(mappedBy = "priority")
	private List<Message> messages;

	public Priority() {
	}

	public Long getIdPriority() {
		return this.idPriority;
	}

	public void setIdPriority(Long idPriority) {
		this.idPriority = idPriority;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public Message addMessage(Message message) {
		getMessages().add(message);
		message.setPriority(this);

		return message;
	}

	public Message removeMessage(Message message) {
		getMessages().remove(message);
		message.setPriority(null);

		return message;
	}

}