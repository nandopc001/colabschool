package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the user_message database table.
 * 
 */
@Entity
@Table(name = "user_message")
@NamedQuery(name = "UserMessage.findAll", query = "SELECT u FROM UserMessage u")
public class UserMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserMessagePK id;

	// bi-directional many-to-one association to Message
	@ManyToOne
	@JoinColumn(name = "id_message", nullable = false, insertable = false, updatable = false)
	private Message message;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false, insertable = false, updatable = false)
	private User user;

	public UserMessage() {
	}

	public UserMessagePK getId() {
		return this.id;
	}

	public void setId(UserMessagePK id) {
		this.id = id;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}