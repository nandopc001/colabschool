package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the profile_role database table.
 * 
 */
@Embeddable
public class ProfileRolePK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "id_profile", insertable = false, updatable = false, unique = true, nullable = false)
	private String idProfile;

	@Column(name = "id_role", insertable = false, updatable = false, unique = true, nullable = false)
	private String idRole;

	public ProfileRolePK() {
	}

	public String getIdProfile() {
		return this.idProfile;
	}

	public void setIdProfile(String idProfile) {
		this.idProfile = idProfile;
	}

	public String getIdRole() {
		return this.idRole;
	}

	public void setIdRole(String idRole) {
		this.idRole = idRole;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ProfileRolePK)) {
			return false;
		}
		ProfileRolePK castOther = (ProfileRolePK) other;
		return this.idProfile.equals(castOther.idProfile) && this.idRole.equals(castOther.idRole);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idProfile.hashCode();
		hash = hash * prime + this.idRole.hashCode();

		return hash;
	}
}