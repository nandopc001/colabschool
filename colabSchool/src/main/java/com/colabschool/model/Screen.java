package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the screen database table.
 * 
 */
@Entity
@Table(name = "screen")
@NamedQuery(name = "Screen.findAll", query = "SELECT s FROM Screen s")
public class Screen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_screen", unique = true, nullable = false)
	private Long idScreen;

	@Column(name = "key_screen", nullable = false, length = 25)
	private String keyScreen;

	@Column(nullable = false, length = 25)
	private String url;

	@Column(nullable = true, length = 25)
	private String form;

	@Column(name = "data_table", nullable = true, length = 25)
	private String dataTable;

	@Column(name = "panel_collapse", nullable = true, length = 25)
	private String panelCollapse;

	// bi-directional many-to-many association to Role
	@ManyToMany
	@JoinTable(name = "screen_role", joinColumns = {
			@JoinColumn(name = "id_screen", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_role", nullable = false) })
	private List<Role> roles;

	public Screen() {
	}

	public Long getIdScreen() {
		return this.idScreen;
	}

	public void setIdScreen(Long idScreen) {
		this.idScreen = idScreen;
	}

	public String getKeyScreen() {
		return this.keyScreen;
	}

	public void setKeyScreen(String keyScreen) {
		this.keyScreen = keyScreen;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	/**
	 * @return the form
	 */
	public String getForm() {
		return form;
	}

	/**
	 * @param form
	 *            the form to set
	 */
	public void setForm(String form) {
		this.form = form;
	}

	/**
	 * @return the dataTable
	 */
	public String getDataTable() {
		return dataTable;
	}

	/**
	 * @param dataTable
	 *            the dataTable to set
	 */
	public void setDataTable(String dataTable) {
		this.dataTable = dataTable;
	}

	/**
	 * @return the panelCollapse
	 */
	public String getPanelCollapse() {
		return panelCollapse;
	}

	/**
	 * @param panelCollapse
	 *            the panelCollapse to set
	 */
	public void setPanelCollapse(String panelCollapse) {
		this.panelCollapse = panelCollapse;
	}

}