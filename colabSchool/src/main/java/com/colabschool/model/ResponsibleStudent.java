package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the responsible_student database table.
 * 
 */
@Entity
@Table(name = "responsible_student")
@NamedQuery(name = "ResponsibleStudent.findAll", query = "SELECT r FROM ResponsibleStudent r")
public class ResponsibleStudent implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ResponsibleStudentPK id;

	// bi-directional many-to-one association to Responsible
	@ManyToOne
	@JoinColumn(name = "id_responsible", nullable = false, insertable = false, updatable = false)
	private Responsible responsible;

	// bi-directional many-to-one association to Student
	@ManyToOne
	@JoinColumn(name = "id_student", nullable = false, insertable = false, updatable = false)
	private Student student;

	public ResponsibleStudent() {
	}

	public ResponsibleStudentPK getId() {
		return this.id;
	}

	public void setId(ResponsibleStudentPK id) {
		this.id = id;
	}

	public Responsible getResponsible() {
		return this.responsible;
	}

	public void setResponsible(Responsible responsible) {
		this.responsible = responsible;
	}

	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}