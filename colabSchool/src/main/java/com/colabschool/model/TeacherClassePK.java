package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the teacher_classe database table.
 * 
 */
@Embeddable
public class TeacherClassePK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "id_teacher", insertable = false, updatable = false, unique = true, nullable = false)
	private String idTeacher;

	@Column(name = "id_classe", insertable = false, updatable = false, unique = true, nullable = false)
	private String idClasse;

	public TeacherClassePK() {
	}

	public String getIdTeacher() {
		return this.idTeacher;
	}

	public void setIdTeacher(String idTeacher) {
		this.idTeacher = idTeacher;
	}

	public String getIdClasse() {
		return this.idClasse;
	}

	public void setIdClasse(String idClasse) {
		this.idClasse = idClasse;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TeacherClassePK)) {
			return false;
		}
		TeacherClassePK castOther = (TeacherClassePK) other;
		return this.idTeacher.equals(castOther.idTeacher) && this.idClasse.equals(castOther.idClasse);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idTeacher.hashCode();
		hash = hash * prime + this.idClasse.hashCode();

		return hash;
	}
}