package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the teacher database table.
 * 
 */
@Entity
@Table(name = "teacher")
@NamedQuery(name = "Teacher.findAll", query = "SELECT t FROM Teacher t")
public class Teacher implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_teacher", unique = true, nullable = false)
	private Long idTeacher;

	// bi-directional many-to-many association to Classe
	@ManyToMany
	@JoinTable(name = "teacher_classe", joinColumns = {
			@JoinColumn(name = "id_teacher", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_classe", nullable = false) })
	private List<Classe> classes;

	// bi-directional many-to-many association to Stuff
	@ManyToMany
	@JoinTable(name = "teacher_stuff", joinColumns = {
			@JoinColumn(name = "id_teacher", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_stuff", nullable = false) })
	private List<Stuff> stuffs;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false)
	private User user;

	// bi-directional many-to-one association to TeacherClasse
	@OneToMany(mappedBy = "teacher1")
	private List<TeacherClasse> teacherClasses1;

	// bi-directional many-to-one association to TeacherClasse
	@OneToMany(mappedBy = "teacher2")
	private List<TeacherClasse> teacherClasses2;

	// bi-directional many-to-one association to TeacherStuff
	@OneToMany(mappedBy = "teacher")
	private List<TeacherStuff> teacherStuffs;

	public Teacher() {
	}

	public Long getIdTeacher() {
		return this.idTeacher;
	}

	public void setIdTeacher(Long idTeacher) {
		this.idTeacher = idTeacher;
	}

	public List<Classe> getClasses() {
		return this.classes;
	}

	public void setClasses(List<Classe> classes) {
		this.classes = classes;
	}

	public List<Stuff> getStuffs() {
		return this.stuffs;
	}

	public void setStuffs(List<Stuff> stuffs) {
		this.stuffs = stuffs;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<TeacherClasse> getTeacherClasses1() {
		return this.teacherClasses1;
	}

	public void setTeacherClasses1(List<TeacherClasse> teacherClasses1) {
		this.teacherClasses1 = teacherClasses1;
	}

	public TeacherClasse addTeacherClasses1(TeacherClasse teacherClasses1) {
		getTeacherClasses1().add(teacherClasses1);
		teacherClasses1.setTeacher1(this);

		return teacherClasses1;
	}

	public TeacherClasse removeTeacherClasses1(TeacherClasse teacherClasses1) {
		getTeacherClasses1().remove(teacherClasses1);
		teacherClasses1.setTeacher1(null);

		return teacherClasses1;
	}

	public List<TeacherClasse> getTeacherClasses2() {
		return this.teacherClasses2;
	}

	public void setTeacherClasses2(List<TeacherClasse> teacherClasses2) {
		this.teacherClasses2 = teacherClasses2;
	}

	public TeacherClasse addTeacherClasses2(TeacherClasse teacherClasses2) {
		getTeacherClasses2().add(teacherClasses2);
		teacherClasses2.setTeacher2(this);

		return teacherClasses2;
	}

	public TeacherClasse removeTeacherClasses2(TeacherClasse teacherClasses2) {
		getTeacherClasses2().remove(teacherClasses2);
		teacherClasses2.setTeacher2(null);

		return teacherClasses2;
	}

	public List<TeacherStuff> getTeacherStuffs() {
		return this.teacherStuffs;
	}

	public void setTeacherStuffs(List<TeacherStuff> teacherStuffs) {
		this.teacherStuffs = teacherStuffs;
	}

	public TeacherStuff addTeacherStuff(TeacherStuff teacherStuff) {
		getTeacherStuffs().add(teacherStuff);
		teacherStuff.setTeacher(this);

		return teacherStuff;
	}

	public TeacherStuff removeTeacherStuff(TeacherStuff teacherStuff) {
		getTeacherStuffs().remove(teacherStuff);
		teacherStuff.setTeacher(null);

		return teacherStuff;
	}

}