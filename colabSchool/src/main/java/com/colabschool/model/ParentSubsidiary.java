package com.colabschool.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the parent_subsidiary database table.
 * 
 */
@Entity
@Table(name = "parent_subsidiary")
@NamedQuery(name = "ParentSubsidiary.findAll", query = "SELECT p FROM ParentSubsidiary p")
public class ParentSubsidiary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_parent_subsidiary", unique = true, nullable = false)
	private Long idParentSubsidiary;

	@Column(length = 18)
	private String cnpj;

	@Column(name = "corporate_name", length = 255)
	private String corporateName;

	@Column(name = "id_contact", nullable = false)
	private Long idContact;

	@Column(name = "inscricao_estadual", length = 14)
	private String inscricaoEstadual;

	@Column(name = "inscricao_municipal", length = 15)
	private String inscricaoMunicipal;

	private BigInteger parent;

	// bi-directional many-to-one association to Adress
	@ManyToOne
	@JoinColumn(name = "id_adress", nullable = false)
	private Adress adress;

	// bi-directional many-to-one association to School
	@ManyToOne
	@JoinColumn(name = "id_school", nullable = false)
	private School school;

	// bi-directional many-to-one association to User
	@OneToMany(mappedBy = "parentSubsidiary")
	private List<User> users;

	public ParentSubsidiary() {
	}

	public Long getIdParentSubsidiary() {
		return this.idParentSubsidiary;
	}

	public void setIdParentSubsidiary(Long idParentSubsidiary) {
		this.idParentSubsidiary = idParentSubsidiary;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCorporateName() {
		return this.corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public Long getIdContact() {
		return this.idContact;
	}

	public void setIdContact(Long idContact) {
		this.idContact = idContact;
	}

	public String getInscricaoEstadual() {
		return this.inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoMunicipal() {
		return this.inscricaoMunicipal;
	}

	public void setInscricaoMunicipal(String inscricaoMunicipal) {
		this.inscricaoMunicipal = inscricaoMunicipal;
	}

	public BigInteger getParent() {
		return this.parent;
	}

	public void setParent(BigInteger parent) {
		this.parent = parent;
	}

	public Adress getAdress() {
		return this.adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public School getSchool() {
		return this.school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setParentSubsidiary(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setParentSubsidiary(null);

		return user;
	}

}