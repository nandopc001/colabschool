package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the profile database table.
 * 
 */
@Entity
@Table(name = "profile")
@NamedQuery(name = "Profile.findAll", query = "SELECT p FROM Profile p")
public class Profile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_profile", unique = true, nullable = false)
	private Long idProfile;

	@Column(nullable = false, length = 100)
	private String description;

	@Column(name = "profile_name", nullable = false, length = 100)
	private String profileName;

	// bi-directional many-to-many association to Role
	@ManyToMany
	@JoinTable(name = "profile_role", joinColumns = {
			@JoinColumn(name = "id_profile", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_role", nullable = false) })
	private List<Role> roles;

	// bi-directional many-to-one association to ProfileRole
	@OneToMany(mappedBy = "profile")
	private List<ProfileRole> profileRoles;

	// bi-directional many-to-one association to User
	@OneToMany(mappedBy = "profile")
	private List<User> users;

	public Profile() {
	}

	public Long getIdProfile() {
		return this.idProfile;
	}

	public void setIdProfile(Long idProfile) {
		this.idProfile = idProfile;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProfileName() {
		return this.profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<ProfileRole> getProfileRoles() {
		return this.profileRoles;
	}

	public void setProfileRoles(List<ProfileRole> profileRoles) {
		this.profileRoles = profileRoles;
	}

	public ProfileRole addProfileRole(ProfileRole profileRole) {
		getProfileRoles().add(profileRole);
		profileRole.setProfile(this);

		return profileRole;
	}

	public ProfileRole removeProfileRole(ProfileRole profileRole) {
		getProfileRoles().remove(profileRole);
		profileRole.setProfile(null);

		return profileRole;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setProfile(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setProfile(null);

		return user;
	}

}