package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the grade database table.
 * 
 */
@Entity
@Table(name = "grade")
@NamedQuery(name = "Grade.findAll", query = "SELECT g FROM Grade g")
public class Grade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_grade", unique = true, nullable = false)
	private Long idGrade;

	@Column(length = 45)
	private String description;

	// bi-directional many-to-one association to Classe
	@OneToMany(mappedBy = "grade")
	private List<Classe> classes;

	public Grade() {
	}

	public Long getIdGrade() {
		return this.idGrade;
	}

	public void setIdGrade(Long idGrade) {
		this.idGrade = idGrade;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Classe> getClasses() {
		return this.classes;
	}

	public void setClasses(List<Classe> classes) {
		this.classes = classes;
	}

	public Classe addClass(Classe classe) {
		getClasses().add(classe);
		classe.setGrade(this);

		return classe;
	}

	public Classe removeClass(Classe classe) {
		getClasses().remove(classe);
		classe.setGrade(null);

		return classe;
	}

}