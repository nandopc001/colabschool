package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the responsible database table.
 * 
 */
@Entity
@Table(name = "responsible")
@NamedQuery(name = "Responsible.findAll", query = "SELECT r FROM Responsible r")
public class Responsible implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_responsible", unique = true, nullable = false)
	private Long idResponsible;

	@Column(length = 45)
	private String kinship;

	// bi-directional many-to-many association to Student
	@ManyToMany
	@JoinTable(name = "responsible_student", joinColumns = {
			@JoinColumn(name = "id_responsible", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_student", nullable = false) })
	private List<Student> students;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false)
	private User user;

	// bi-directional many-to-one association to ResponsibleStudent
	@OneToMany(mappedBy = "responsible")
	private List<ResponsibleStudent> responsibleStudents;

	public Responsible() {
	}

	public Long getIdResponsible() {
		return this.idResponsible;
	}

	public void setIdResponsible(Long idResponsible) {
		this.idResponsible = idResponsible;
	}

	public String getKinship() {
		return this.kinship;
	}

	public void setKinship(String kinship) {
		this.kinship = kinship;
	}

	public List<Student> getStudents() {
		return this.students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<ResponsibleStudent> getResponsibleStudents() {
		return this.responsibleStudents;
	}

	public void setResponsibleStudents(List<ResponsibleStudent> responsibleStudents) {
		this.responsibleStudents = responsibleStudents;
	}

	public ResponsibleStudent addResponsibleStudent(ResponsibleStudent responsibleStudent) {
		getResponsibleStudents().add(responsibleStudent);
		responsibleStudent.setResponsible(this);

		return responsibleStudent;
	}

	public ResponsibleStudent removeResponsibleStudent(ResponsibleStudent responsibleStudent) {
		getResponsibleStudents().remove(responsibleStudent);
		responsibleStudent.setResponsible(null);

		return responsibleStudent;
	}

}