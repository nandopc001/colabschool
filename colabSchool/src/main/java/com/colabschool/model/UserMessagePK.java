package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the user_message database table.
 * 
 */
@Embeddable
public class UserMessagePK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "id_message", insertable = false, updatable = false, unique = true, nullable = false)
	private String idMessage;

	@Column(name = "id_user", insertable = false, updatable = false, unique = true, nullable = false)
	private String idUser;

	public UserMessagePK() {
	}

	public String getIdMessage() {
		return this.idMessage;
	}

	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}

	public String getIdUser() {
		return this.idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserMessagePK)) {
			return false;
		}
		UserMessagePK castOther = (UserMessagePK) other;
		return this.idMessage.equals(castOther.idMessage) && this.idUser.equals(castOther.idUser);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idMessage.hashCode();
		hash = hash * prime + this.idUser.hashCode();

		return hash;
	}
}