package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the classe_stuff database table.
 * 
 */
@Entity
@Table(name = "classe_stuff")
@NamedQuery(name = "ClasseStuff.findAll", query = "SELECT c FROM ClasseStuff c")
public class ClasseStuff implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ClasseStuffPK id;

	// bi-directional many-to-one association to Classe
	@ManyToOne
	@JoinColumn(name = "id_classe", nullable = false, insertable = false, updatable = false)
	private Classe classe1;

	// bi-directional many-to-one association to Classe
	@ManyToOne
	@JoinColumn(name = "id_classe", nullable = false, insertable = false, updatable = false)
	private Classe classe2;

	// bi-directional many-to-one association to Stuff
	@ManyToOne
	@JoinColumn(name = "id_stuff", nullable = false, insertable = false, updatable = false)
	private Stuff stuff1;

	// bi-directional many-to-one association to Stuff
	@ManyToOne
	@JoinColumn(name = "id_stuff", nullable = false, insertable = false, updatable = false)
	private Stuff stuff2;

	public ClasseStuff() {
	}

	public ClasseStuffPK getId() {
		return this.id;
	}

	public void setId(ClasseStuffPK id) {
		this.id = id;
	}

	public Classe getClasse1() {
		return this.classe1;
	}

	public void setClasse1(Classe classe1) {
		this.classe1 = classe1;
	}

	public Classe getClasse2() {
		return this.classe2;
	}

	public void setClasse2(Classe classe2) {
		this.classe2 = classe2;
	}

	public Stuff getStuff1() {
		return this.stuff1;
	}

	public void setStuff1(Stuff stuff1) {
		this.stuff1 = stuff1;
	}

	public Stuff getStuff2() {
		return this.stuff2;
	}

	public void setStuff2(Stuff stuff2) {
		this.stuff2 = stuff2;
	}

}