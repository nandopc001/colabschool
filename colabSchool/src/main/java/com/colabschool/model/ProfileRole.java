package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the profile_role database table.
 * 
 */
@Entity
@Table(name = "profile_role")
@NamedQuery(name = "ProfileRole.findAll", query = "SELECT p FROM ProfileRole p")
public class ProfileRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProfileRolePK id;

	// bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name = "id_profile", nullable = false, insertable = false, updatable = false)
	private Profile profile;

	// bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name = "id_role", nullable = false, insertable = false, updatable = false)
	private Role role;

	public ProfileRole() {
	}

	public ProfileRolePK getId() {
		return this.id;
	}

	public void setId(ProfileRolePK id) {
		this.id = id;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}