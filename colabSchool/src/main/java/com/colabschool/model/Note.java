package com.colabschool.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the note database table.
 * 
 */
@Entity
@Table(name = "note")
@NamedQuery(name = "Note.findAll", query = "SELECT n FROM Note n")
public class Note implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_note", unique = true, nullable = false)
	private Long idNote;

	@Column(name = "note_number", precision = 10)
	private BigDecimal noteNumber;

	@Column(name = "note_string", length = 2)
	private String noteString;

	// bi-directional many-to-one association to Student
	@ManyToOne
	@JoinColumn(name = "id_student", nullable = false)
	private Student student;

	// bi-directional many-to-one association to Stuff
	@ManyToOne
	@JoinColumn(name = "id_stuff", nullable = false)
	private Stuff stuff;

	public Note() {
	}

	public Long getIdNote() {
		return this.idNote;
	}

	public void setIdNote(Long idNote) {
		this.idNote = idNote;
	}

	public BigDecimal getNoteNumber() {
		return this.noteNumber;
	}

	public void setNoteNumber(BigDecimal noteNumber) {
		this.noteNumber = noteNumber;
	}

	public String getNoteString() {
		return this.noteString;
	}

	public void setNoteString(String noteString) {
		this.noteString = noteString;
	}

	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Stuff getStuff() {
		return this.stuff;
	}

	public void setStuff(Stuff stuff) {
		this.stuff = stuff;
	}

}