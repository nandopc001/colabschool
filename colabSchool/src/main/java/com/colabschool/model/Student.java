package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the student database table.
 * 
 */
@Entity
@Table(name = "student")
@NamedQuery(name = "Student.findAll", query = "SELECT s FROM Student s")
public class Student implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_student", unique = true, nullable = false)
	private Long idStudent;

	// bi-directional many-to-one association to Note
	@OneToMany(mappedBy = "student")
	private List<Note> notes;

	// bi-directional many-to-many association to Responsible
	@ManyToMany(mappedBy = "students")
	private List<Responsible> responsibles;

	// bi-directional many-to-one association to ResponsibleStudent
	@OneToMany(mappedBy = "student")
	private List<ResponsibleStudent> responsibleStudents;

	// bi-directional many-to-one association to Classe
	@ManyToOne
	@JoinColumn(name = "id_classe", nullable = false)
	private Classe classe;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "id_user", nullable = false)
	private User user;

	public Student() {
	}

	public Long getIdStudent() {
		return this.idStudent;
	}

	public void setIdStudent(Long idStudent) {
		this.idStudent = idStudent;
	}

	public List<Note> getNotes() {
		return this.notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Note addNote(Note note) {
		getNotes().add(note);
		note.setStudent(this);

		return note;
	}

	public Note removeNote(Note note) {
		getNotes().remove(note);
		note.setStudent(null);

		return note;
	}

	public List<Responsible> getResponsibles() {
		return this.responsibles;
	}

	public void setResponsibles(List<Responsible> responsibles) {
		this.responsibles = responsibles;
	}

	public List<ResponsibleStudent> getResponsibleStudents() {
		return this.responsibleStudents;
	}

	public void setResponsibleStudents(List<ResponsibleStudent> responsibleStudents) {
		this.responsibleStudents = responsibleStudents;
	}

	public ResponsibleStudent addResponsibleStudent(ResponsibleStudent responsibleStudent) {
		getResponsibleStudents().add(responsibleStudent);
		responsibleStudent.setStudent(this);

		return responsibleStudent;
	}

	public ResponsibleStudent removeResponsibleStudent(ResponsibleStudent responsibleStudent) {
		getResponsibleStudents().remove(responsibleStudent);
		responsibleStudent.setStudent(null);

		return responsibleStudent;
	}

	public Classe getClasse() {
		return this.classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}