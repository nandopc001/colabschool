package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the stuff database table.
 * 
 */
@Entity
@Table(name = "stuff")
@NamedQuery(name = "Stuff.findAll", query = "SELECT s FROM Stuff s")
public class Stuff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_stuff", unique = true, nullable = false)
	private Long idStuff;

	@Column(length = 45)
	private String description;

	// bi-directional many-to-many association to Classe
	@ManyToMany(mappedBy = "stuffs")
	private List<Classe> classes;

	// bi-directional many-to-one association to ClasseStuff
	@OneToMany(mappedBy = "stuff1")
	private List<ClasseStuff> classeStuffs1;

	// bi-directional many-to-one association to ClasseStuff
	@OneToMany(mappedBy = "stuff2")
	private List<ClasseStuff> classeStuffs2;

	// bi-directional many-to-one association to Note
	@OneToMany(mappedBy = "stuff")
	private List<Note> notes;

	// bi-directional many-to-many association to Teacher
	@ManyToMany(mappedBy = "stuffs")
	private List<Teacher> teachers;

	// bi-directional many-to-one association to TeacherStuff
	@OneToMany(mappedBy = "stuff")
	private List<TeacherStuff> teacherStuffs;

	public Stuff() {
	}

	public Long getIdStuff() {
		return this.idStuff;
	}

	public void setIdStuff(Long idStuff) {
		this.idStuff = idStuff;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Classe> getClasses() {
		return this.classes;
	}

	public void setClasses(List<Classe> classes) {
		this.classes = classes;
	}

	public List<ClasseStuff> getClasseStuffs1() {
		return this.classeStuffs1;
	}

	public void setClasseStuffs1(List<ClasseStuff> classeStuffs1) {
		this.classeStuffs1 = classeStuffs1;
	}

	public ClasseStuff addClasseStuffs1(ClasseStuff classeStuffs1) {
		getClasseStuffs1().add(classeStuffs1);
		classeStuffs1.setStuff1(this);

		return classeStuffs1;
	}

	public ClasseStuff removeClasseStuffs1(ClasseStuff classeStuffs1) {
		getClasseStuffs1().remove(classeStuffs1);
		classeStuffs1.setStuff1(null);

		return classeStuffs1;
	}

	public List<ClasseStuff> getClasseStuffs2() {
		return this.classeStuffs2;
	}

	public void setClasseStuffs2(List<ClasseStuff> classeStuffs2) {
		this.classeStuffs2 = classeStuffs2;
	}

	public ClasseStuff addClasseStuffs2(ClasseStuff classeStuffs2) {
		getClasseStuffs2().add(classeStuffs2);
		classeStuffs2.setStuff2(this);

		return classeStuffs2;
	}

	public ClasseStuff removeClasseStuffs2(ClasseStuff classeStuffs2) {
		getClasseStuffs2().remove(classeStuffs2);
		classeStuffs2.setStuff2(null);

		return classeStuffs2;
	}

	public List<Note> getNotes() {
		return this.notes;
	}

	public void setNotes(List<Note> notes) {
		this.notes = notes;
	}

	public Note addNote(Note note) {
		getNotes().add(note);
		note.setStuff(this);

		return note;
	}

	public Note removeNote(Note note) {
		getNotes().remove(note);
		note.setStuff(null);

		return note;
	}

	public List<Teacher> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	public List<TeacherStuff> getTeacherStuffs() {
		return this.teacherStuffs;
	}

	public void setTeacherStuffs(List<TeacherStuff> teacherStuffs) {
		this.teacherStuffs = teacherStuffs;
	}

	public TeacherStuff addTeacherStuff(TeacherStuff teacherStuff) {
		getTeacherStuffs().add(teacherStuff);
		teacherStuff.setStuff(this);

		return teacherStuff;
	}

	public TeacherStuff removeTeacherStuff(TeacherStuff teacherStuff) {
		getTeacherStuffs().remove(teacherStuff);
		teacherStuff.setStuff(null);

		return teacherStuff;
	}

}