package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the classe_stuff database table.
 * 
 */
@Embeddable
public class ClasseStuffPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "id_classe", insertable = false, updatable = false, unique = true, nullable = false)
	private String idClasse;

	@Column(name = "id_stuff", insertable = false, updatable = false, unique = true, nullable = false)
	private String idStuff;

	public ClasseStuffPK() {
	}

	public String getIdClasse() {
		return this.idClasse;
	}

	public void setIdClasse(String idClasse) {
		this.idClasse = idClasse;
	}

	public String getIdStuff() {
		return this.idStuff;
	}

	public void setIdStuff(String idStuff) {
		this.idStuff = idStuff;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ClasseStuffPK)) {
			return false;
		}
		ClasseStuffPK castOther = (ClasseStuffPK) other;
		return this.idClasse.equals(castOther.idClasse) && this.idStuff.equals(castOther.idStuff);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idClasse.hashCode();
		hash = hash * prime + this.idStuff.hashCode();

		return hash;
	}
}