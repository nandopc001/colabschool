package com.colabschool.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@Table(name = "user")
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_user", unique = true, nullable = false)
	private Long idUser;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_of_birth")
	private Date dateOfBirth;

	@Column(nullable = false, length = 255)
	private String email;

	@Column(length = 1)
	private String gender;

	@Column(length = 255)
	private String name;

	@Column(length = 255)
	private String password;

	// bi-directional many-to-one association to Responsible
	@OneToMany(mappedBy = "user")
	private List<Responsible> responsibles;

	// bi-directional many-to-one association to Student
	@OneToMany(mappedBy = "user")
	private List<Student> students;

	// bi-directional many-to-one association to Teacher
	@OneToMany(mappedBy = "user")
	private List<Teacher> teachers;

	// bi-directional many-to-one association to Adress
	@ManyToOne
	@JoinColumn(name = "id_adress", nullable = false)
	private Adress adress;

	// bi-directional many-to-one association to Contact
	@ManyToOne
	@JoinColumn(name = "id_contact", nullable = false)
	private Contact contact;

	// bi-directional many-to-many association to Message
	@ManyToMany
	@JoinTable(name = "user_message", joinColumns = {
			@JoinColumn(name = "id_user", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_message", nullable = false) })
	private List<Message> messages;

	// bi-directional many-to-one association to ParentSubsidiary
	@ManyToOne
	@JoinColumn(name = "id_parent_subsidiary")
	private ParentSubsidiary parentSubsidiary;

	// bi-directional many-to-one association to Profile
	@ManyToOne
	@JoinColumn(name = "id_profile", nullable = false)
	private Profile profile;

	// bi-directional many-to-many association to Role
	@ManyToMany
	@JoinTable(name = "user_role", joinColumns = {
			@JoinColumn(name = "id_user", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_role", nullable = false) })
	private List<Role> roles;

	// bi-directional many-to-one association to UserMessage
	@OneToMany(mappedBy = "user")
	private List<UserMessage> userMessages;

	// bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy = "user1")
	private List<UserRole> userRoles1;

	// bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy = "user2")
	private List<UserRole> userRoles2;

	public User() {
	}

	public Long getIdUser() {
		return this.idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Responsible> getResponsibles() {
		return this.responsibles;
	}

	public void setResponsibles(List<Responsible> responsibles) {
		this.responsibles = responsibles;
	}

	public Responsible addResponsible(Responsible responsible) {
		getResponsibles().add(responsible);
		responsible.setUser(this);

		return responsible;
	}

	public Responsible removeResponsible(Responsible responsible) {
		getResponsibles().remove(responsible);
		responsible.setUser(null);

		return responsible;
	}

	public List<Student> getStudents() {
		return this.students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Student addStudent(Student student) {
		getStudents().add(student);
		student.setUser(this);

		return student;
	}

	public Student removeStudent(Student student) {
		getStudents().remove(student);
		student.setUser(null);

		return student;
	}

	public List<Teacher> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	public Teacher addTeacher(Teacher teacher) {
		getTeachers().add(teacher);
		teacher.setUser(this);

		return teacher;
	}

	public Teacher removeTeacher(Teacher teacher) {
		getTeachers().remove(teacher);
		teacher.setUser(null);

		return teacher;
	}

	public Adress getAdress() {
		return this.adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}

	public Contact getContact() {
		return this.contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public ParentSubsidiary getParentSubsidiary() {
		return this.parentSubsidiary;
	}

	public void setParentSubsidiary(ParentSubsidiary parentSubsidiary) {
		this.parentSubsidiary = parentSubsidiary;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<Role> getRoles() {
		return this.roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public List<UserMessage> getUserMessages() {
		return this.userMessages;
	}

	public void setUserMessages(List<UserMessage> userMessages) {
		this.userMessages = userMessages;
	}

	public UserMessage addUserMessage(UserMessage userMessage) {
		getUserMessages().add(userMessage);
		userMessage.setUser(this);

		return userMessage;
	}

	public UserMessage removeUserMessage(UserMessage userMessage) {
		getUserMessages().remove(userMessage);
		userMessage.setUser(null);

		return userMessage;
	}

	public List<UserRole> getUserRoles1() {
		return this.userRoles1;
	}

	public void setUserRoles1(List<UserRole> userRoles1) {
		this.userRoles1 = userRoles1;
	}

	public UserRole addUserRoles1(UserRole userRoles1) {
		getUserRoles1().add(userRoles1);
		userRoles1.setUser1(this);

		return userRoles1;
	}

	public UserRole removeUserRoles1(UserRole userRoles1) {
		getUserRoles1().remove(userRoles1);
		userRoles1.setUser1(null);

		return userRoles1;
	}

	public List<UserRole> getUserRoles2() {
		return this.userRoles2;
	}

	public void setUserRoles2(List<UserRole> userRoles2) {
		this.userRoles2 = userRoles2;
	}

	public UserRole addUserRoles2(UserRole userRoles2) {
		getUserRoles2().add(userRoles2);
		userRoles2.setUser2(this);

		return userRoles2;
	}

	public UserRole removeUserRoles2(UserRole userRoles2) {
		getUserRoles2().remove(userRoles2);
		userRoles2.setUser2(null);

		return userRoles2;
	}

}