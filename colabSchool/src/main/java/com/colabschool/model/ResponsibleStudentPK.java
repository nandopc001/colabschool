package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the responsible_student database table.
 * 
 */
@Embeddable
public class ResponsibleStudentPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "id_responsible", insertable = false, updatable = false, unique = true, nullable = false)
	private String idResponsible;

	@Column(name = "id_student", insertable = false, updatable = false, unique = true, nullable = false)
	private String idStudent;

	public ResponsibleStudentPK() {
	}

	public String getIdResponsible() {
		return this.idResponsible;
	}

	public void setIdResponsible(String idResponsible) {
		this.idResponsible = idResponsible;
	}

	public String getIdStudent() {
		return this.idStudent;
	}

	public void setIdStudent(String idStudent) {
		this.idStudent = idStudent;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ResponsibleStudentPK)) {
			return false;
		}
		ResponsibleStudentPK castOther = (ResponsibleStudentPK) other;
		return this.idResponsible.equals(castOther.idResponsible) && this.idStudent.equals(castOther.idStudent);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idResponsible.hashCode();
		hash = hash * prime + this.idStudent.hashCode();

		return hash;
	}
}