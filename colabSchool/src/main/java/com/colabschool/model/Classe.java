package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the classe database table.
 * 
 */
@Entity
@Table(name = "classe")
@NamedQuery(name = "Classe.findAll", query = "SELECT c FROM Classe c")
public class Classe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_classe", unique = true, nullable = false)
	private Long idClasse;

	@Column(length = 45)
	private String description;

	// bi-directional many-to-one association to Grade
	@ManyToOne
	@JoinColumn(name = "id_grade", nullable = false)
	private Grade grade;

	// bi-directional many-to-many association to Stuff
	@ManyToMany
	@JoinTable(name = "classe_stuff", joinColumns = {
			@JoinColumn(name = "id_classe", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "id_stuff", nullable = false) })
	private List<Stuff> stuffs;

	// bi-directional many-to-one association to ClasseStuff
	@OneToMany(mappedBy = "classe1")
	private List<ClasseStuff> classeStuffs1;

	// bi-directional many-to-one association to ClasseStuff
	@OneToMany(mappedBy = "classe2")
	private List<ClasseStuff> classeStuffs2;

	// bi-directional many-to-one association to Student
	@OneToMany(mappedBy = "classe")
	private List<Student> students;

	// bi-directional many-to-many association to Teacher
	@ManyToMany(mappedBy = "classes")
	private List<Teacher> teachers;

	// bi-directional many-to-one association to TeacherClasse
	@OneToMany(mappedBy = "classe1")
	private List<TeacherClasse> teacherClasses1;

	// bi-directional many-to-one association to TeacherClasse
	@OneToMany(mappedBy = "classe2")
	private List<TeacherClasse> teacherClasses2;

	public Classe() {
	}

	public Long getIdClasse() {
		return this.idClasse;
	}

	public void setIdClasse(Long idClasse) {
		this.idClasse = idClasse;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Grade getGrade() {
		return this.grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public List<Stuff> getStuffs() {
		return this.stuffs;
	}

	public void setStuffs(List<Stuff> stuffs) {
		this.stuffs = stuffs;
	}

	public List<ClasseStuff> getClasseStuffs1() {
		return this.classeStuffs1;
	}

	public void setClasseStuffs1(List<ClasseStuff> classeStuffs1) {
		this.classeStuffs1 = classeStuffs1;
	}

	public ClasseStuff addClasseStuffs1(ClasseStuff classeStuffs1) {
		getClasseStuffs1().add(classeStuffs1);
		classeStuffs1.setClasse1(this);

		return classeStuffs1;
	}

	public ClasseStuff removeClasseStuffs1(ClasseStuff classeStuffs1) {
		getClasseStuffs1().remove(classeStuffs1);
		classeStuffs1.setClasse1(null);

		return classeStuffs1;
	}

	public List<ClasseStuff> getClasseStuffs2() {
		return this.classeStuffs2;
	}

	public void setClasseStuffs2(List<ClasseStuff> classeStuffs2) {
		this.classeStuffs2 = classeStuffs2;
	}

	public ClasseStuff addClasseStuffs2(ClasseStuff classeStuffs2) {
		getClasseStuffs2().add(classeStuffs2);
		classeStuffs2.setClasse2(this);

		return classeStuffs2;
	}

	public ClasseStuff removeClasseStuffs2(ClasseStuff classeStuffs2) {
		getClasseStuffs2().remove(classeStuffs2);
		classeStuffs2.setClasse2(null);

		return classeStuffs2;
	}

	public List<Student> getStudents() {
		return this.students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Student addStudent(Student student) {
		getStudents().add(student);
		student.setClasse(this);

		return student;
	}

	public Student removeStudent(Student student) {
		getStudents().remove(student);
		student.setClasse(null);

		return student;
	}

	public List<Teacher> getTeachers() {
		return this.teachers;
	}

	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}

	public List<TeacherClasse> getTeacherClasses1() {
		return this.teacherClasses1;
	}

	public void setTeacherClasses1(List<TeacherClasse> teacherClasses1) {
		this.teacherClasses1 = teacherClasses1;
	}

	public TeacherClasse addTeacherClasses1(TeacherClasse teacherClasses1) {
		getTeacherClasses1().add(teacherClasses1);
		teacherClasses1.setClasse1(this);

		return teacherClasses1;
	}

	public TeacherClasse removeTeacherClasses1(TeacherClasse teacherClasses1) {
		getTeacherClasses1().remove(teacherClasses1);
		teacherClasses1.setClasse1(null);

		return teacherClasses1;
	}

	public List<TeacherClasse> getTeacherClasses2() {
		return this.teacherClasses2;
	}

	public void setTeacherClasses2(List<TeacherClasse> teacherClasses2) {
		this.teacherClasses2 = teacherClasses2;
	}

	public TeacherClasse addTeacherClasses2(TeacherClasse teacherClasses2) {
		getTeacherClasses2().add(teacherClasses2);
		teacherClasses2.setClasse2(this);

		return teacherClasses2;
	}

	public TeacherClasse removeTeacherClasses2(TeacherClasse teacherClasses2) {
		getTeacherClasses2().remove(teacherClasses2);
		teacherClasses2.setClasse2(null);

		return teacherClasses2;
	}

}