package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the teacher_stuff database table.
 * 
 */
@Entity
@Table(name = "teacher_stuff")
@NamedQuery(name = "TeacherStuff.findAll", query = "SELECT t FROM TeacherStuff t")
public class TeacherStuff implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TeacherStuffPK id;

	// bi-directional many-to-one association to Stuff
	@ManyToOne
	@JoinColumn(name = "id_stuff", nullable = false, insertable = false, updatable = false)
	private Stuff stuff;

	// bi-directional many-to-one association to Teacher
	@ManyToOne
	@JoinColumn(name = "id_teacher", nullable = false, insertable = false, updatable = false)
	private Teacher teacher;

	public TeacherStuff() {
	}

	public TeacherStuffPK getId() {
		return this.id;
	}

	public void setId(TeacherStuffPK id) {
		this.id = id;
	}

	public Stuff getStuff() {
		return this.stuff;
	}

	public void setStuff(Stuff stuff) {
		this.stuff = stuff;
	}

	public Teacher getTeacher() {
		return this.teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

}