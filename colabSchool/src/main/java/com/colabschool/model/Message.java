package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the message database table.
 * 
 */
@Entity
@Table(name = "message")
@NamedQuery(name = "Message.findAll", query = "SELECT m FROM Message m")
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_message", unique = true, nullable = false)
	private Long idMessage;

	@Column(nullable = false)
	private String text;

	// bi-directional many-to-one association to Priority
	@ManyToOne
	@JoinColumn(name = "id_priority", nullable = false)
	private Priority priority;

	// bi-directional many-to-many association to User
	@ManyToMany(mappedBy = "messages")
	private List<User> users;

	// bi-directional many-to-one association to UserMessage
	@OneToMany(mappedBy = "message")
	private List<UserMessage> userMessages;

	public Message() {
	}

	public Long getIdMessage() {
		return this.idMessage;
	}

	public void setIdMessage(Long idMessage) {
		this.idMessage = idMessage;
	}

	public String getMessage() {
		return this.text;
	}

	public void setMessage(String message) {
		this.text = message;
	}

	public Priority getPriority() {
		return this.priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<UserMessage> getUserMessages() {
		return this.userMessages;
	}

	public void setUserMessages(List<UserMessage> userMessages) {
		this.userMessages = userMessages;
	}

	public UserMessage addUserMessage(UserMessage userMessage) {
		getUserMessages().add(userMessage);
		userMessage.setMessage(this);

		return userMessage;
	}

	public UserMessage removeUserMessage(UserMessage userMessage) {
		getUserMessages().remove(userMessage);
		userMessage.setMessage(null);

		return userMessage;
	}

}