package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the school database table.
 * 
 */
@Entity
@Table(name = "school")
@NamedQuery(name = "School.findAll", query = "SELECT s FROM School s")
public class School implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_school", unique = true, nullable = false)
	private Long idSchool;

	private String logo;

	@Column(length = 255)
	private String name;

	// bi-directional many-to-one association to ParentSubsidiary
	@OneToMany(mappedBy = "school")
	private List<ParentSubsidiary> parentSubsidiaries;

	public School() {
	}

	public Long getIdSchool() {
		return this.idSchool;
	}

	public void setIdSchool(Long idSchool) {
		this.idSchool = idSchool;
	}

	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ParentSubsidiary> getParentSubsidiaries() {
		return this.parentSubsidiaries;
	}

	public void setParentSubsidiaries(List<ParentSubsidiary> parentSubsidiaries) {
		this.parentSubsidiaries = parentSubsidiaries;
	}

	public ParentSubsidiary addParentSubsidiary(ParentSubsidiary parentSubsidiary) {
		getParentSubsidiaries().add(parentSubsidiary);
		parentSubsidiary.setSchool(this);

		return parentSubsidiary;
	}

	public ParentSubsidiary removeParentSubsidiary(ParentSubsidiary parentSubsidiary) {
		getParentSubsidiaries().remove(parentSubsidiary);
		parentSubsidiary.setSchool(null);

		return parentSubsidiary;
	}

}