package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the teacher_stuff database table.
 * 
 */
@Embeddable
public class TeacherStuffPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "id_teacher", insertable = false, updatable = false, unique = true, nullable = false)
	private String idTeacher;

	@Column(name = "id_stuff", insertable = false, updatable = false, unique = true, nullable = false)
	private String idStuff;

	public TeacherStuffPK() {
	}

	public String getIdTeacher() {
		return this.idTeacher;
	}

	public void setIdTeacher(String idTeacher) {
		this.idTeacher = idTeacher;
	}

	public String getIdStuff() {
		return this.idStuff;
	}

	public void setIdStuff(String idStuff) {
		this.idStuff = idStuff;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TeacherStuffPK)) {
			return false;
		}
		TeacherStuffPK castOther = (TeacherStuffPK) other;
		return this.idTeacher.equals(castOther.idTeacher) && this.idStuff.equals(castOther.idStuff);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idTeacher.hashCode();
		hash = hash * prime + this.idStuff.hashCode();

		return hash;
	}
}