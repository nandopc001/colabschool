package com.colabschool.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the teacher_classe database table.
 * 
 */
@Entity
@Table(name = "teacher_classe")
@NamedQuery(name = "TeacherClasse.findAll", query = "SELECT t FROM TeacherClasse t")
public class TeacherClasse implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TeacherClassePK id;

	// bi-directional many-to-one association to Classe
	@ManyToOne
	@JoinColumn(name = "id_classe", nullable = false, insertable = false, updatable = false)
	private Classe classe1;

	// bi-directional many-to-one association to Classe
	@ManyToOne
	@JoinColumn(name = "id_classe", nullable = false, insertable = false, updatable = false)
	private Classe classe2;

	// bi-directional many-to-one association to Teacher
	@ManyToOne
	@JoinColumn(name = "id_teacher", nullable = false, insertable = false, updatable = false)
	private Teacher teacher1;

	// bi-directional many-to-one association to Teacher
	@ManyToOne
	@JoinColumn(name = "id_teacher", nullable = false, insertable = false, updatable = false)
	private Teacher teacher2;

	public TeacherClasse() {
	}

	public TeacherClassePK getId() {
		return this.id;
	}

	public void setId(TeacherClassePK id) {
		this.id = id;
	}

	public Classe getClasse1() {
		return this.classe1;
	}

	public void setClasse1(Classe classe1) {
		this.classe1 = classe1;
	}

	public Classe getClasse2() {
		return this.classe2;
	}

	public void setClasse2(Classe classe2) {
		this.classe2 = classe2;
	}

	public Teacher getTeacher1() {
		return this.teacher1;
	}

	public void setTeacher1(Teacher teacher1) {
		this.teacher1 = teacher1;
	}

	public Teacher getTeacher2() {
		return this.teacher2;
	}

	public void setTeacher2(Teacher teacher2) {
		this.teacher2 = teacher2;
	}

}