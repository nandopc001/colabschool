package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the contact database table.
 * 
 */
@Entity
@Table(name = "contact")
@NamedQuery(name = "Contact.findAll", query = "SELECT c FROM Contact c")
public class Contact implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_contact", unique = true, nullable = false)
	private Long idContact;

	@Column(length = 255)
	private String email;

	@Column(name = "email_secundary", length = 255)
	private String emailSecundary;

	@Column(name = "phone_cell", length = 17)
	private String phoneCell;

	@Column(name = "phone_home", length = 17)
	private String phoneHome;

	@Column(name = "phone_work", length = 17)
	private String phoneWork;

	// bi-directional many-to-one association to User
	@OneToMany(mappedBy = "contact")
	private List<User> users;

	public Contact() {
	}

	public Long getIdContact() {
		return this.idContact;
	}

	public void setIdContact(Long idContact) {
		this.idContact = idContact;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailSecundary() {
		return this.emailSecundary;
	}

	public void setEmailSecundary(String emailSecundary) {
		this.emailSecundary = emailSecundary;
	}

	public String getPhoneCell() {
		return this.phoneCell;
	}

	public void setPhoneCell(String phoneCell) {
		this.phoneCell = phoneCell;
	}

	public String getPhoneHome() {
		return this.phoneHome;
	}

	public void setPhoneHome(String phoneHome) {
		this.phoneHome = phoneHome;
	}

	public String getPhoneWork() {
		return this.phoneWork;
	}

	public void setPhoneWork(String phoneWork) {
		this.phoneWork = phoneWork;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setContact(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setContact(null);

		return user;
	}

}