package com.colabschool.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the adress database table.
 * 
 */
@Entity
@Table(name = "adress")
@NamedQuery(name = "Adress.findAll", query = "SELECT a FROM Adress a")
public class Adress implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_adress", unique = true, nullable = false)
	private Long idAdress;

	@Column(length = 9)
	private String cep;

	@Column(length = 128)
	private String city;

	@Column(length = 45)
	private String complement;

	@Column(length = 70)
	private String country;

	@Column(length = 60)
	private String district;

	private double latitude;

	private double longitude;

	@Column(precision = 10)
	private BigDecimal number;

	@Column(length = 70)
	private String state;

	@Column(length = 255)
	private String street;

	@Column(length = 9)
	private String zip;

	// bi-directional many-to-one association to ParentSubsidiary
	@OneToMany(mappedBy = "adress")
	private List<ParentSubsidiary> parentSubsidiaries;

	// bi-directional many-to-one association to User
	@OneToMany(mappedBy = "adress")
	private List<User> users;

	public Adress() {
	}

	public Long getIdAdress() {
		return this.idAdress;
	}

	public void setIdAdress(Long idAdress) {
		this.idAdress = idAdress;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getComplement() {
		return this.complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getNumber() {
		return this.number;
	}

	public void setNumber(BigDecimal number) {
		this.number = number;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public List<ParentSubsidiary> getParentSubsidiaries() {
		return this.parentSubsidiaries;
	}

	public void setParentSubsidiaries(List<ParentSubsidiary> parentSubsidiaries) {
		this.parentSubsidiaries = parentSubsidiaries;
	}

	public ParentSubsidiary addParentSubsidiary(ParentSubsidiary parentSubsidiary) {
		getParentSubsidiaries().add(parentSubsidiary);
		parentSubsidiary.setAdress(this);

		return parentSubsidiary;
	}

	public ParentSubsidiary removeParentSubsidiary(ParentSubsidiary parentSubsidiary) {
		getParentSubsidiaries().remove(parentSubsidiary);
		parentSubsidiary.setAdress(null);

		return parentSubsidiary;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setAdress(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setAdress(null);

		return user;
	}

}