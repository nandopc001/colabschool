package com.colabschool.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@Table(name = "role")
@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_role", unique = true, nullable = false)
	private Long idRole;

	@Column(nullable = false, length = 100)
	private String description;

	@Column(nullable = false, length = 100)
	private String functionality;

	// bi-directional many-to-many association to Profile
	@ManyToMany(mappedBy = "roles")
	private List<Profile> profiles;

	// bi-directional many-to-one association to ProfileRole
	@OneToMany(mappedBy = "role")
	private List<ProfileRole> profileRoles;

	// bi-directional many-to-many association to Screen
	@ManyToMany(mappedBy = "roles")
	private List<Screen> screens;

	// bi-directional many-to-many association to User
	@ManyToMany(mappedBy = "roles")
	private List<User> users;

	// bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy = "role1")
	private List<UserRole> userRoles1;

	// bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy = "role2")
	private List<UserRole> userRoles2;

	public Role() {
	}

	public Long getIdRole() {
		return this.idRole;
	}

	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFunctionality() {
		return this.functionality;
	}

	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}

	public List<Profile> getProfiles() {
		return this.profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public List<ProfileRole> getProfileRoles() {
		return this.profileRoles;
	}

	public void setProfileRoles(List<ProfileRole> profileRoles) {
		this.profileRoles = profileRoles;
	}

	public ProfileRole addProfileRole(ProfileRole profileRole) {
		getProfileRoles().add(profileRole);
		profileRole.setRole(this);

		return profileRole;
	}

	public ProfileRole removeProfileRole(ProfileRole profileRole) {
		getProfileRoles().remove(profileRole);
		profileRole.setRole(null);

		return profileRole;
	}

	public List<Screen> getScreens() {
		return this.screens;
	}

	public void setScreens(List<Screen> screens) {
		this.screens = screens;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<UserRole> getUserRoles1() {
		return this.userRoles1;
	}

	public void setUserRoles1(List<UserRole> userRoles1) {
		this.userRoles1 = userRoles1;
	}

	public UserRole addUserRoles1(UserRole userRoles1) {
		getUserRoles1().add(userRoles1);
		userRoles1.setRole1(this);

		return userRoles1;
	}

	public UserRole removeUserRoles1(UserRole userRoles1) {
		getUserRoles1().remove(userRoles1);
		userRoles1.setRole1(null);

		return userRoles1;
	}

	public List<UserRole> getUserRoles2() {
		return this.userRoles2;
	}

	public void setUserRoles2(List<UserRole> userRoles2) {
		this.userRoles2 = userRoles2;
	}

	public UserRole addUserRoles2(UserRole userRoles2) {
		getUserRoles2().add(userRoles2);
		userRoles2.setRole2(this);

		return userRoles2;
	}

	public UserRole removeUserRoles2(UserRole userRoles2) {
		getUserRoles2().remove(userRoles2);
		userRoles2.setRole2(null);

		return userRoles2;
	}

}