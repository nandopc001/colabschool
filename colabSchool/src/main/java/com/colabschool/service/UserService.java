/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import com.colabschool.model.User;

/**
 * @author nandopc001
 *
 */
public interface UserService {

	List<User> findAll();

	User findUserByIdUser(Long idUser);

	User findUserByName(String name);

	User save(User user);

	void delete(Long idUser);

	User findUserByEmail(String email);

	void saveUser(User user);
}
