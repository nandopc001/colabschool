/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.ResponsibleStudent;

/**
 * @author nandopc001
 *
 */
public interface ResponsibleStudentService {
	ResponsibleStudent save(ResponsibleStudent responsibleStudent);
}
