/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.ResponsibleStudentPK;

/**
 * @author nandopc001
 *
 */
public interface ResponsibleStudentPKService {
	ResponsibleStudentPK save(ResponsibleStudentPK responsibleStudentPK);
}
