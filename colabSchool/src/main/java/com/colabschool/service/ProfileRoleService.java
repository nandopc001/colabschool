/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.ProfileRole;

/**
 * @author nandopc001
 *
 */
public interface ProfileRoleService {
	ProfileRole save(ProfileRole profileRole);
}
