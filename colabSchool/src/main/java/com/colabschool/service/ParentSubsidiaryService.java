/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import com.colabschool.model.ParentSubsidiary;

/**
 * @author nandopc001
 *
 */
public interface ParentSubsidiaryService {
	ParentSubsidiary save(ParentSubsidiary parentSubsidiary);

	List<ParentSubsidiary> findAll();
}
