/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import com.colabschool.model.Classe;

/**
 * @author nandopc001
 *
 */
public interface ClasseService {
	Classe save(Classe classe);

	List<Classe> findAll();
}
