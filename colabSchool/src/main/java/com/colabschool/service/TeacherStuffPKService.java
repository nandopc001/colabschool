/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.TeacherStuffPK;

/**
 * @author nandopc001
 *
 */
public interface TeacherStuffPKService {
	TeacherStuffPK save(TeacherStuffPK teacherStuffPK);
}
