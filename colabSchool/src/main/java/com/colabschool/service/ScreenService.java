/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import com.colabschool.model.Screen;

/**
 * @author nandopc001
 *
 */
public interface ScreenService {

	Screen save(Screen screen);

	List<Screen> findAll();

	Screen findOne(Long idScreen);

	Screen findByKeyScreen(String keyScreen);

}
