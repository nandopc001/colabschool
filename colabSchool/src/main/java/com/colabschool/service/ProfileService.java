/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import com.colabschool.model.Profile;

/**
 * @author nandopc001
 *
 */
public interface ProfileService {
	Profile save(Profile profile);

	Profile findByDescription(String description);

	List<Profile> findAll();
}
