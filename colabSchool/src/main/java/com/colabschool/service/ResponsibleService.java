/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Responsible;

/**
 * @author nandopc001
 *
 */
public interface ResponsibleService {
	Responsible save(Responsible responsible);
}
