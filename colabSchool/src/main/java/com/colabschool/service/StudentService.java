/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Student;

/**
 * @author nandopc001
 *
 */
public interface StudentService {
	Student save(Student student);
}
