/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.ResponsibleStudent;
import com.colabschool.repository.ResponsibleStudentRepository;
import com.colabschool.service.ResponsibleStudentService;

/**
 * @author nandopc001
 *
 */
@Service
public class ResponsibleStudentServiceImpl implements ResponsibleStudentService {
	@Autowired
	ResponsibleStudentRepository responsibleStudentRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.ResponsibleStudentService#save(com.colabschool.
	 * model.ResponsibleStudent)
	 */
	@Override
	public ResponsibleStudent save(ResponsibleStudent responsibleStudent) {
		// TODO Auto-generated method stub
		return responsibleStudentRepository.save(responsibleStudent);
	}

}
