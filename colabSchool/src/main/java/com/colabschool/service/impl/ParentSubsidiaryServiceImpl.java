/**
 * 
 */
package com.colabschool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.ParentSubsidiary;
import com.colabschool.repository.ParentSubsidiaryRepository;
import com.colabschool.service.ParentSubsidiaryService;

/**
 * @author nandopc001
 *
 */
@Service
public class ParentSubsidiaryServiceImpl implements ParentSubsidiaryService {
	@Autowired
	private ParentSubsidiaryRepository parentSubsidiaryRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.ParentSubsidiaryService#save(com.colabschool.
	 * model.ParentSubsidiary)
	 */
	@Override
	public ParentSubsidiary save(ParentSubsidiary parentSubsidiary) {
		// TODO Auto-generated method stub
		return parentSubsidiaryRepository.save(parentSubsidiary);
	}

	@Override
	public List<ParentSubsidiary> findAll() {
		// TODO Auto-generated method stub
		return parentSubsidiaryRepository.findAll();
	}

}
