/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.TeacherStuff;
import com.colabschool.repository.TeacherStuffRepository;
import com.colabschool.service.TeacherStuffService;

/**
 * @author nandopc001
 *
 */
@Service
public class TeacherStuffServiceImpl implements TeacherStuffService {
	@Autowired
	TeacherStuffRepository teacherStuffRepository;

	@Override
	public TeacherStuff save(TeacherStuff teacherStuff) {
		// TODO Auto-generated method stub
		return teacherStuffRepository.save(teacherStuff);
	}

}
