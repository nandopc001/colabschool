/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Message;
import com.colabschool.repository.MessageRepository;
import com.colabschool.service.MessageService;

/**
 * @author nandopc001
 *
 */
@Service
public class MessageServiceImpl implements MessageService {
	@Autowired
	private MessageRepository messageRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.MessageService#save(com.colabschool.model.
	 * Message)
	 */
	@Override
	public Message save(Message message) {
		// TODO Auto-generated method stub
		return messageRepository.save(message);
	}

}
