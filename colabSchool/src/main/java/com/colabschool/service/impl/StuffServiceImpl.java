/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Stuff;
import com.colabschool.repository.StuffRepository;
import com.colabschool.service.StuffService;

/**
 * @author nandopc001
 *
 */
@Service
public class StuffServiceImpl implements StuffService {
	@Autowired
	StuffRepository stuffRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.StuffService#save(com.colabschool.model.Stuff)
	 */
	@Override
	public Stuff save(Stuff stuff) {
		// TODO Auto-generated method stub
		return stuffRepository.save(stuff);
	}

}
