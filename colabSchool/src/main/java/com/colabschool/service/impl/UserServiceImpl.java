/**
 * 
 */
package com.colabschool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.colabschool.model.Profile;
import com.colabschool.model.User;
import com.colabschool.repository.ProfileRepository;
import com.colabschool.repository.UserRepository;
import com.colabschool.service.UserService;

/**
 * @author nandopc001
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProfileRepository profileRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findUserByIdUser(Long idUser) {
		// TODO Auto-generated method stub
		return userRepository.findByIdUser(idUser);
	}

	@Override
	public User save(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public void delete(Long idUser) {
		// TODO Auto-generated method stub
		userRepository.delete(idUser);
	}

	@Override
	public User findUserByName(String name) {
		// TODO Auto-generated method stub
		return userRepository.findByName(name);
	}

	@Override
	public User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(email);
	}

	@Override
	public void saveUser(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		// user.setActive(1);
		Profile userProfile = profileRepository.findByDescription("ADMIN");
		user.setProfile(userProfile);
		userRepository.save(user);

	}

}
