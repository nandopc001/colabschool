/**
 * 
 */
package com.colabschool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Classe;
import com.colabschool.repository.ClasseRepository;
import com.colabschool.service.ClasseService;

/**
 * @author nandopc001
 *
 */
@Service
public class ClasseServiceImpl implements ClasseService {

	@Autowired
	private ClasseRepository classeRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.ClasseService#save(com.colabschool.model.Classe)
	 */
	@Override
	public Classe save(Classe classe) {
		// TODO Auto-generated method stub
		return classeRepository.save(classe);
	}

	@Override
	public List<Classe> findAll() {
		// TODO Auto-generated method stub
		return classeRepository.findAll();
	}

}
