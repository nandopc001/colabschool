/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Teacher;
import com.colabschool.repository.TeacherRepository;
import com.colabschool.service.TeacherService;

/**
 * @author nandopc001
 *
 */
@Service
public class TeacherServiceImpl implements TeacherService {
	@Autowired
	TeacherRepository teacherRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.TeacherService#save(com.colabschool.model.
	 * Teacher)
	 */
	@Override
	public Teacher save(Teacher teacher) {
		// TODO Auto-generated method stub
		return teacherRepository.save(teacher);
	}

}
