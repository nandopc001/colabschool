/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Student;
import com.colabschool.repository.StudentRepository;
import com.colabschool.service.StudentService;

/**
 * @author nandopc001
 *
 */
@Service
public class StudentServiceImpl implements StudentService {
	@Autowired
	StudentRepository studentRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.StudentService#save(com.colabschool.model.
	 * Student)
	 */
	@Override
	public Student save(Student student) {
		// TODO Auto-generated method stub
		return studentRepository.save(student);
	}

}
