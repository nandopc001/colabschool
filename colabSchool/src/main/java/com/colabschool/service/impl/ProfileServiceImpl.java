/**
 * 
 */
package com.colabschool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Profile;
import com.colabschool.repository.ProfileRepository;
import com.colabschool.service.ProfileService;

/**
 * @author nandopc001
 *
 */
@Service
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	ProfileRepository profileRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.ProfileService#save(java.lang.String)
	 */
	@Override
	public Profile save(Profile profile) {
		// TODO Auto-generated method stub
		return profileRepository.save(profile);
	}

	@Override
	public Profile findByDescription(String description) {
		// TODO Auto-generated method stub
		return profileRepository.findByDescription(description);
	}

	@Override
	public List<Profile> findAll() {
		// TODO Auto-generated method stub
		return profileRepository.findAll();
	}

}
