/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Responsible;
import com.colabschool.repository.ResponsibleRepository;
import com.colabschool.service.ResponsibleService;

/**
 * @author nandopc001
 *
 */
@Service
public class ResponsibleServiceImpl implements ResponsibleService {
	@Autowired
	private ResponsibleRepository responsibleRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.ResponsibleService#save(com.colabschool.model.
	 * Responsible)
	 */
	@Override
	public Responsible save(Responsible responsible) {
		// TODO Auto-generated method stub
		return responsibleRepository.save(responsible);
	}

}
