/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Adress;
import com.colabschool.repository.AdressRepository;
import com.colabschool.service.AdressService;

/**
 * @author nandopc001
 *
 */
@Service
public class AdressServiceImpl implements AdressService {

	@Autowired
	AdressRepository adressRepository;

	@Override
	public Adress save(Adress adress) {
		// TODO Auto-generated method stub
		return adressRepository.save(adress);
	}

}
