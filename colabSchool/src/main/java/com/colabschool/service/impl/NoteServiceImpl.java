/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Note;
import com.colabschool.repository.NoteRepository;
import com.colabschool.service.NoteService;

/**
 * @author nandopc001
 *
 */
@Service
public class NoteServiceImpl implements NoteService {
	@Autowired
	private NoteRepository noteRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.NoteService#save(com.colabschool.model.Note)
	 */
	@Override
	public Note save(Note note) {
		// TODO Auto-generated method stub
		return noteRepository.save(note);
	}

}
