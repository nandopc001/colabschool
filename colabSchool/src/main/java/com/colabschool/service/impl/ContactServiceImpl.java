package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Contact;
import com.colabschool.repository.ContactRepository;
import com.colabschool.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	private ContactRepository contactRepository;

	@Override
	public Contact findContactByEmail(String email) {
		// TODO Auto-generated method stub
		return contactRepository.findByEmail(email);
	}

	@Override
	public Contact save(Contact contact) {
		// TODO Auto-generated method stub
		return contactRepository.save(contact);
	}

}
