/**
 * 
 */
package com.colabschool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Role;
import com.colabschool.repository.RoleRepository;
import com.colabschool.service.RoleService;

/**
 * @author nandopc001
 *
 */
@Service
public class RoleServiceImpl implements RoleService {
	@Autowired
	RoleRepository roleRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.RoleService#save(com.colabschool.model.Role)
	 */
	@Override
	public Role save(Role role) {
		// TODO Auto-generated method stub
		return roleRepository.save(role);
	}

	@Override
	public List<Role> findAll() {
		// TODO Auto-generated method stub
		return roleRepository.findAll();
	}

	@Override
	public Role findOne(Long idRole) {
		// TODO Auto-generated method stub
		return roleRepository.findOne(idRole);
	}

}
