/**
 * 
 */
package com.colabschool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Grade;
import com.colabschool.repository.GradeRepository;
import com.colabschool.service.GradeService;

/**
 * @author nandopc001
 *
 */
@Service
public class GradeServiceImpl implements GradeService {

	@Autowired
	private GradeRepository gradeRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.GradeService#save(com.colabschool.model.Grade)
	 */
	@Override
	public Grade save(Grade grade) {
		// TODO Auto-generated method stub
		return gradeRepository.save(grade);
	}

	@Override
	public List<Grade> findAll() {
		// TODO Auto-generated method stub
		return gradeRepository.findAll();
	}

}
