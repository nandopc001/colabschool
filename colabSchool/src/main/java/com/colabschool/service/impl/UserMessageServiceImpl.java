/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.UserMessage;
import com.colabschool.repository.UserMessageRepository;
import com.colabschool.service.UserMessageService;

/**
 * @author nandopc001
 *
 */
@Service
public class UserMessageServiceImpl implements UserMessageService {
	@Autowired
	UserMessageRepository userMessageRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.UserMessageService#save(com.colabschool.model.
	 * UserMessage)
	 */
	@Override
	public UserMessage save(UserMessage userMessage) {
		// TODO Auto-generated method stub
		return userMessageRepository.save(userMessage);
	}

}
