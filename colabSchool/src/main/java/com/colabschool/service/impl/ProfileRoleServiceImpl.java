/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.ProfileRole;
import com.colabschool.repository.ProfileRoleRepository;
import com.colabschool.service.ProfileRoleService;

/**
 * @author nandopc001
 *
 */
@Service
public class ProfileRoleServiceImpl implements ProfileRoleService {
	@Autowired
	private ProfileRoleRepository profileRoleRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.ProfileRoleService#save(com.colabschool.model.
	 * ProfileRole)
	 */
	@Override
	public ProfileRole save(ProfileRole profileRole) {
		// TODO Auto-generated method stub
		return profileRoleRepository.save(profileRole);
	}

}
