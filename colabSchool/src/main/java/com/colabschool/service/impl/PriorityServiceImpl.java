/**
 * 
 */
package com.colabschool.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Priority;
import com.colabschool.repository.PriorityRepository;
import com.colabschool.service.PriorityService;

/**
 * @author nandopc001
 *
 */
@Service
public class PriorityServiceImpl implements PriorityService {
	@Autowired
	PriorityRepository priorityRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.PriorityService#save(com.colabschool.model.
	 * Priority)
	 */
	@Override
	public Priority save(Priority priority) {
		// TODO Auto-generated method stub
		return priorityRepository.save(priority);
	}

}
