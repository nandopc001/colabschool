/**
 * 
 */
package com.colabschool.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.Screen;
import com.colabschool.repository.ScreenRepository;
import com.colabschool.service.ScreenService;

/**
 * @author nandopc001
 *
 */
@Service
public class ScreenServiceImpl implements ScreenService {

	@Autowired
	ScreenRepository screenRepository;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.colabschool.service.ScreenService#save(com.colabschool.model.Screen)
	 */
	@Override
	public Screen save(Screen screen) {
		// TODO Auto-generated method stub
		return screenRepository.save(screen);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.ScreenService#findAll()
	 */
	@Override
	public List<Screen> findAll() {
		// TODO Auto-generated method stub
		return screenRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.colabschool.service.ScreenService#findOne(java.lang.Long)
	 */
	@Override
	public Screen findOne(Long idScreen) {
		// TODO Auto-generated method stub
		return screenRepository.findOne(idScreen);
	}

	@Override
	public Screen findByKeyScreen(String keyScreen) {
		// TODO Auto-generated method stub
		return screenRepository.findByKeyScreen(keyScreen);
	}

}
