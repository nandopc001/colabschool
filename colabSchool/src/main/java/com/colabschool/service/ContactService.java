/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Contact;

/**
 * @author nandopc001
 *
 */
public interface ContactService {

	Contact findContactByEmail(String email);

	Contact save(Contact contact);

}
