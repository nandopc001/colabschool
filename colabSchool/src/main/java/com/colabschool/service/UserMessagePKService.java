/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.UserMessagePK;

/**
 * @author nandopc001
 *
 */
public interface UserMessagePKService {
	UserMessagePK save(UserMessagePK userMessagePK);
}
