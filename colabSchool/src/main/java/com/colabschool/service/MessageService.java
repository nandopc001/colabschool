/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Message;

/**
 * @author nandopc001
 *
 */
public interface MessageService {
	Message save(Message message);
}
