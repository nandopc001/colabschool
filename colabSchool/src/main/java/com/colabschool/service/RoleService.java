/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import com.colabschool.model.Role;

/**
 * @author nandopc001
 *
 */
public interface RoleService {

	Role save(Role role);

	List<Role> findAll();

	Role findOne(Long idRole);
}
