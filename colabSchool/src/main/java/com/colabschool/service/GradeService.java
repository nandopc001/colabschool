/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import com.colabschool.model.Grade;

/**
 * @author nandopc001
 *
 */
public interface GradeService {

	Grade save(Grade grade);

	List<Grade> findAll();
}
