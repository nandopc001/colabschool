/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.TeacherStuff;

/**
 * @author nandopc001
 *
 */
public interface TeacherStuffService {
	TeacherStuff save(TeacherStuff teacherStuff);
}
