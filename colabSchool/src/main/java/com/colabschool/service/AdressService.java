/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Adress;

/**
 * @author nandopc001
 *
 */
public interface AdressService {

	Adress save(Adress adress);
}
