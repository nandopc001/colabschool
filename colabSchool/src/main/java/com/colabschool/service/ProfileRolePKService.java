/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.ProfileRolePK;

/**
 * @author nandopc001
 *
 */
public interface ProfileRolePKService {
	ProfileRolePK save(ProfileRolePK profileRolePK);

}
