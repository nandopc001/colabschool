/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Teacher;

/**
 * @author nandopc001
 *
 */
public interface TeacherService {
	Teacher save(Teacher teacher);
}
