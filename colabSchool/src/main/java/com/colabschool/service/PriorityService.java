/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Priority;

/**
 * @author nandopc001
 *
 */
public interface PriorityService {
	Priority save(Priority priority);
}
