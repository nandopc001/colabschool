/**
 * 
 */
package com.colabschool.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.colabschool.model.School;
import com.colabschool.repository.SchoolRepository;

/**
 * @author nandopc001
 *
 */
@Service
public class SchoolService {

	@Autowired
	private SchoolRepository schoolRepository;

	public List<School> findAll() {
		// TODO Auto-generated method stub
		return schoolRepository.findAll();
	}

	public School findOne(Long idSchool) {
		// TODO Auto-generated method stub
		return schoolRepository.findOne(idSchool);
	}

	public School save(School school) {
		// TODO Auto-generated method stub
		return schoolRepository.save(school);
	}

	public void delete(Long idSchool) {
		// TODO Auto-generated method stub
		schoolRepository.delete(idSchool);
	}

}
