/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.UserMessage;

/**
 * @author nandopc001
 *
 */
public interface UserMessageService {
	UserMessage save(UserMessage userMessage);
}
