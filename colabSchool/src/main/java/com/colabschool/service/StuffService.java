/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Stuff;

/**
 * @author nandopc001
 *
 */
public interface StuffService {
	Stuff save(Stuff stuff);
}
