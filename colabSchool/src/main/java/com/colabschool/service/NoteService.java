/**
 * 
 */
package com.colabschool.service;

import com.colabschool.model.Note;

/**
 * @author nandopc001
 *
 */
public interface NoteService {

	Note save(Note note);

}
