/**
 * 
 */
package com.colabschool.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * @author nandopc001
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private DataSource dataSource;

	@Value("${spring.queries.users-query}")
	private String usersQuery;

	@Value("${spring.queries.profile-query}")
	private String profilesQuery;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().usersByUsernameQuery(usersQuery).authoritiesByUsernameQuery(profilesQuery)
				.dataSource(dataSource).passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/login").permitAll().antMatchers("/forms")
				.permitAll().antMatchers("/cad_classe").permitAll().antMatchers("/cad_grade").permitAll()
				.antMatchers("/cad_responsible").permitAll().antMatchers("/cad_teacher").permitAll()
				.antMatchers("/cad_student").permitAll().antMatchers("/cad_screen1").permitAll()
				.antMatchers("/cad_screen").permitAll().antMatchers("/registration").permitAll()
				.antMatchers("/cad_user").permitAll().antMatchers("/cad_profile").permitAll().antMatchers("/cad_role")
				.permitAll().antMatchers("/cad_parent_subsid").permitAll().antMatchers("/index").permitAll()
				.antMatchers("/save_school").permitAll().antMatchers("/blank").permitAll().antMatchers("/cad_school")
				.permitAll().antMatchers("/admin/**").hasAuthority("ADMIN").anyRequest().authenticated().and().csrf()
				.disable().formLogin().loginPage("/login").failureUrl("/login?error=true")
				.defaultSuccessUrl("/admin/home").usernameParameter("email").passwordParameter("password").and()
				.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/").and()
				.exceptionHandling().accessDeniedPage("/access-denied");
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**", "/static/**", "/vendor/**", "/dist/**", "/data/**", "/css/**",
				"/js/**", "/images/**");
	}

}
