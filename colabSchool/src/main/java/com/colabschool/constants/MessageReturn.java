/**
 * 
 */
package com.colabschool.constants;

/**
 * @author nandopc001
 *
 */
public enum MessageReturn {
	SUCESS("0", "Cadastro efetuado com sucesso"), ERROR("1", "Falha no cadastro");
	private String code;
	private String description;

	MessageReturn(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String code() {
		return code;
	}

	public String description() {
		return description;
	}
}
