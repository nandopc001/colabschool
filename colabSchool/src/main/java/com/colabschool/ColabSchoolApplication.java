package com.colabschool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColabSchoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(ColabSchoolApplication.class, args);
	}
}
